#!/usr/bin/env python
"""
this script is used to map the light intensities measured by he calibration plate ( in mV) for a set of DAC value for a single led and store the data into a csv file.
"""
__author__ = 'Bruno Favie  <bruno.favie@student.uva.nl>'
__version__ = "2016:01:12 12:00"


import  drivers.u6control_raw as u6control
import drivers.wellcontrol as wellcontrol
import csv
import time
cal = u6control.board()
wc = wellcontrol.wcontrol()
channellist = [7,3,6,2,5,1,4,0]
port = 'COM3'
wc.connectBoard0(port)
def run(port,device,well,color):
  
  file = open('calibrationplate data/data well '+ str(well) +' '+ color +'.txt', 'w')
  writer = csv.writer(file)
  line = ["DAC","value"]
  writer.writerow(line) # appends line to list.
  
  colordict = {"blue":0,"green":1,"red":2,"white":3}
  lednr = (well-1)*4
  (board,dev,chan) = wc.led_to_bdc(lednr+colordict[color])
  #measuring each value
  print "running calibration of well "+str(well)+" "+ color + " led"
  for i in range(0,4097,10):
    if i in range(0,4000,100):
      print i # printing progress marker for every 10 measurements
    wc.leddac(0,dev, channellist[chan], value = i)
    time.sleep(.1)
    line = [i,cal.measure(well,color)]
    writer.writerow(line) # appends line to list.
    wc.leddac(0, dev, channellist[chan], 0)
    time.sleep(.1)


#MANUAL RUN
if __name__ == "__main__":
    device = "1"
    for well in [15]:
      for color in ["green","blue"]:
        run(port,device,well,color)
