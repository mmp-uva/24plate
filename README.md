# 24.plate

24.Plate is a collecion of Python scripts designed to control and calbrate the LEDs and photodiodes from the 24-well microplate incubator.

It is designed to be run from a terminal window or to be executed trough the Graphical User Interface.

## Requirements

* LabJack U6 pro drivers & software[^1]
* numpy
* cPickle
* Pyserial
* PyQT4

## Usage

Execute Wellplate_controller.py from a Terminal window in order to control the LEDs and photodiodes trough the command line. Execute GUI.py in order to activate the Graphical User Interface.
Both the interface and command line allow for control of LED intensities, photodiode measurements and the saving and loading of LED configurations.

calibrate_leds.py and calibrate_sensors.py use the calibrationplate, connected trough the Labjack U6 pro unit to calibrate the individual LEDS.

### Configuration

".cal" files created with Qpickle are used to store and load the calibration data of each setup, and are stored in the respective subfolder in the devices folder.
The last used light settings of each device is stored in the leds.leds file, which is read by the system when (re)starting a device.

### Using the tools
scripts found in the main directory:

* `calibrate_leds.py` Calibration of the leds using the calibration plate.
* `calibrate_sensors.py` Calibration of the photodiodes using calibrated LEDs.
* `fakeledcalibrator.py` Creates a fake calibration file for the LEDs, usable when calibration scripts are inoperatible.
* `fakesensorcalibrator.py` Creates a fake photodiode calibration file for the photodiodes, usable when the calibration scripts are inoperatible.
* `GUI.py` Graphical user interface script.
* `O2measure.py` placeholder script for O2 measurement protocol
* `ODmeasure.py` placeholder script for OD measurement
* `photometer csv.py` Script used to store current returned by calibration plate for certain led itensities, based on DAC values. Used for calibration of calibrationplate.
* `Serailports.py` Script containing data on serialports of each device. Adjust when serial ports of electronics have changed.
* `wellcontroller.py` Script containing all functions for a single well
* `wellplate_controller.py` Script containing all functions for controlling the LEDs and photodiodes. Can be executed from terminal.

Scripts found in drivers folder:

* `portchecker.py` Returns used serial ports.
* `testu6.py` Returns sensor value from selected well of the calibration plate.
* `u6.py` Driver containing basic functions of the Labjack U6 pro for use wih python.
* `u6control.py` Driver written for use of the calibrationplate. Depends on U6.py
* `u6control_raw.py` Identical to u6control.py, except that the returned values are no adjusted for photodiode sensitivity variations.
* `wellcontrol.py` Driver used to control the LEDs and photodiodes by communicating with the MOJO V3 unit.

# Footnotes

[^1]: https://labjack.com/products/u6
