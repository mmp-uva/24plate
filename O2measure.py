#!/usr/bin/env python
"""
O2measure.py contains all the functions required for performing an OD measurement using the 24 well microplate incubator.
It can be called upon by other scripts but can also be executed by itself, allowing for quick measurements trough a console window.
When executed on itself it will store it's data based on the first argument given, which is used as the experiment name.
The second argument used is the number of the device.

Example:

Python O2measure.py experiment_name #device
"""

__author__ = 'Bruno Favie  <bruno.favie@student.uva.nl>'
__version__ = "2015:12:08 16:00"

import sys
import time
import wellcontroller
import wellplate24csv
import cPickle as pickle

#setting experiment name. If no name given "TEMP" is used.
if len(sys.argv) >1:
	experiment = (sys.argv[1])
else:
	experiment = "TEMP"    
		

class O2measure():
		"""
		This class contains functions which interact directly with the O2 leds and the wellcontroller.
		When executed as the main script it will preform an O2 measurement of each well and store the data.
		"""

		def __init__(self):
				pass
				

		def O2_pulse(self,wellnumber,wc,device):# THIS IS A PLACEHOLDER SCRIPT!!! Actual protocol not yet tested!
								"""
								turns off al leds connected to a specific well before pulsing the Blue led for a O2 measurement.
								Returns the sensor value
								loads the led values from file and restores illumination.
								@param wellnumber: selected well
								@type wellnumber: integer
								@param wc instance of wellcontrol.py used for cummunicating with the hardware.
								@type instance
								@param device: device number
								@type string
								"""
								self.device = device
								wc.well_off(wellnumber)
								time.sleep(0.01) # time for allowing the sensor to reset
								#turn on blue led.
								wc.led("blue",50,wellnumber)
								#saturation period
								time.sleep(1)
								#Turning off the led .
								wc.led("blue",0,wellnumber)
								#measuring emission time.
								time = wc.relax()
								#converting time into O2 value
								measurement = time * 1 #<--- ADD VARIABLES HERE!!
								#load previous led settings and restor illumination.
								bgrw = pickle.load( open( "devices/"+self.device+"/"+"leds.leds", "rb" ))
								wc.well_restore(wellnumber,bgrw)
								#return measurement
								return measurement

if __name__ == '__main__':
	"""
	Executes when script is called from a terminal/console window.
	"""
	
	#selecting experiment name. If no name given "TEMP" is used.
	if len(sys.argv) >1:
		experiment = (sys.argv[1])
	else:
		experiment = "TEMP"
	#selecting devicse used for measurement. If no device is given a prompt will be given to enter a device number in the terminal
	if len(sys.argv) >2:
		device = (sys.argv[2])
	else:
		device = raw_input("Select device number: ")
	print "Running O2 measurement for", experiment
	#a serialports for connecting to the incubator setup.
	wc = wellcontroller.wellcontroller()
	O2 = O2measure()
	measurements = [None]*48
	# subtract disabled wells from total number of wells.
	wells = wc.activewell(range(1,49))
	#run measurements for all active wells wells.
	print "measuring..."
	for i in wells:
		measurements[i-1] = O2.O2_pulse(i,wc)
	print "... done"
	#store measurements
	print "storing data..."
	csv = wellplate24csv.csvfileedit()
	csv.appendrow(experiment,'O2', measurements)
	print "... done"

	#selecting device. If no device is given a promt will be produced in terminal
	if len(sys.argv) >2:
		device = (sys.argv[2])
	else:
		device = raw_input("Select device number: ")
	print "Running O2 measurement for", experiment
	#set COMports for driver.
	wc = wellcontroller.wellcontroller(device)
	OD = ODmeasure()
	measurements = [None]*48
	# subtract disabled wells from total number of wells.
	wells = wc.activewell(range(1,49))
	#run measurements for all active wells wells.
	print "measuring..."
	for i in wells:
		measurements[i-1] = OD.OD_pulse(i,wc) 
	print "... done"
	#store measurements
	print "storing data..."
	csv = wellplate24csv.csvfileedit()
	csv.appendrow(experiment,'OD', measurements)
	print "... done"