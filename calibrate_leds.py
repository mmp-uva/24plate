#!/usr/bin/env python
"""
Responsible for calibrating the leds by converting DAC values to mE/m^2*s
Uses the calibration plate connected to a labjack u6-pro for measuring light intensities of each led, determining boundaries and convering set of values into a polynomial.
Stores results into the ledcalibration.cal file of the device

"""

__author__ = 'Bruno Favie <bruno.favie@student.uva.nl>'
__version__ = "6-FIBER-SYSTEM VERSION! 2016:01:25 12:00"
import sys
import drivers.u6control as u6control
import wellcontroller
from serial import Serial  
import cPickle as pickle
import numpy as np

class calibration():
	"""
	class that contains the functions required for the led calibration.
	"""
	def __init__(self,device):
		#connecting to the calibration plate
		self.calibration = u6control.board()
		self.device = device
		#list for storing the 192 sets of polynomial variables.
		self.ledcalibration = [[0,0]]*192
		self.colordict = {"blue":0,"green":1,"red":2,"white":3}
	
	def set_calibrationfile(self):
		"""creating a calibrationfile with direct value troughput( value * 1 + 0) in order to send pure DAC values to the system"""
		llist = [[1,0]]*192 # 1*DAC + 0 = DAC
		pickle.dump(llist,open( "devices/"+self.device+"/"+'ledcalibration.cal', "wb" ))

	def read_sensor(self,well,color):
		"""
		returns photodiode value from selected well.
		@param well: well number 1..24
		@type  well: int
		@param color: catagory led measured
		@type color: string
		"""
		return self.calibration.measure(well,color)

		
	def measurement(self,wc,side,plate):
		"""
		Function which determines the active wells for the selected plate, sets the different color and executes the calibration of each led.
		stores the results returned by the calibration plate in a dictionary.
		@param wc: well number 1..24
		@type  wc: int
		@param side: part of the wellplate unit being calibrated
		@type side: string
		@param plate: number of the welplate unit being calibrated
		@type plate: integer
		"""
		#list of wells
		if plate == 0:
			self.wells = wc.activewell(range(1,25))
		elif plate == 1:
			self.wells = wc.activewell(range(25,49))
		if plate not in [0,1] or self.wells == -1:
			print "No wells found. Plate is disconnected or no valid plate selected"
			return
		#Selecing colors based on side measured. halts if incorrect side variable is entered.
		if side == "bed":
			color = ["green","blue"]
		elif side == "cover":
			color = ["red","white"]
		else:
			print "Incorrect measurement position given. Calibration plate should ether face the 'bed'or the 'cover'of the incubator."
			return
				
		"""Calibration steps"""
		for w in self.wells: # for each active well
			for c in color: # for each active color
				results = [] #list for storing initial results of measurements.
						
				"""Step 1: determining upper boundary DAC"""
				# USED PRESET MAXIMUM VALUE! CHANGE [Threshold] VALUE IF NEEDED
				Threshold = 500
				step = 1000 # Initial set size.
				DAC = 4000 # setting DAC to upper level for determining the upper limit.
				while step >= 1: # repeat while step is greater then 1.
					delta_DAC = DAC - step # setting delta-DAC value.
					wc.led(c,delta_DAC,w)# set led
					reading =  self.read_sensor(w,c) # read intensity
					if reading > threshold: # if measurement is still above threshold
						DAC = DAC - step # reduce DAC value with step size
					else:
						step = step / 10 # reduce step size if value is below threshold.
				Top = DAC # Storing DAC value used to achieve the threshold.
				
				"""step 2: Gathering measurement point between the offset value and the threshold value"""
				step = 500 # set size used for measurement points.
				domain = range(30,Top,step)
				domain.append(Top)# ensuring that the threshold value is added at the end of the domain.
				for x in domain:
					wc.led(c,x,w)# set led
					results.append(self.read_sensor(w,c)) # read intensity and stores in results list.

				"""step 3: Determining the relation between DAC values and Light intensity."""
				x = domain
				y = results
				#creating polyfit
				fit =  np.polyfit(x,y,1)
				# obtaing led number based on color and well.
				led = ((w-1)*4)+colordict[c]
				#storing polynomial variables as a list in the dictionary.
				ledcalibration[led] = list(fit)
				
			pickle.dump(ledcalibration,open( "devices/"+self.device+"/"+'ledcalibration.cal', "wb" ))
	
	def runcalibration(self,device):
		"""
		Function which runs the measurements for each part of the device, storing the values and calculating the relation between DAC values and the light intensities
		stores the calibration data into a .cal file
		"""
		self.device = device
		wc = wellcontroller.wellcontroller(self.device)
		#write blanco calibration file
		self.set_calibrationfile()
		#load new calibrationfile.
		wc.load_ledcalibration()
		#instructions
		
		#1rst plate
		print '\n'+'\n'+ "Insert the calibration plate between the wellplate cover and the wellplate of the first microplate."+'\n'+"Point the leds directed downwards towards the wellplate bed."+'\n'+"Make sure that the setup is secured."+'\n'
		raw_input("Press any key to continue...")
		print "Measuring..."
		self.measurement(wc,"bed",plate =0)
		#instructions
		print '\n'+'\n'+ "Insert the calibration plate between the wellplate and the wellplate bed of the first microplate."+'\n'+ "Point leds directed upwards towards the wellplate cover."+'\n'+"Make sure that the setup is secured."+'\n'
		raw_input("Press any key to continue...")
		print "Measuring..."
		self.measurement(wc,"cover",plate =0)
		
		#2nd plate
		print '\n'+'\n'+ "Insert the calibration plate between the wellplate cover and the wellplate bed of the second microplate."+'\n'+"Point the leds directed downwards towards the wellplate bed."+'\n'+"Make sure that the setup is secured."+'\n'
		raw_input("Press any key to continue...")
		print "Measuring..."
		self.measurement(wc,"bed",plate =1)
		#instructions
		print '\n'+'\n'+ "Insert the calibration plate between the wellplate and the wellplate bed of the second microplate."+'\n'+ "Point leds directed upwards towards the wellplate cover."+'\n'+"Make sure that the setup is secured."+'\n'
		raw_input("Press any key to continue...")
		print "Measuring..."
		self.measurement(wc,"cover",plate =1)

if __name__ == "__main__":
		#selecting device to be calibrated
		device = raw_input("Select device number: ")
		#creating initial calibration file with direct value troughput
		c = calibration(device)
		c.set_calibrationfile
		c.runcalibration(device)





