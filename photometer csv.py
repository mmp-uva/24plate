#!/usr/bin/env python
"""
this script is used to map the light intensities from the photometer for a set of dac values for a single led and store the data into a csv file.
"""
__author__ = 'Bruno Favie  <bruno.favie@student.uva.nl>'
__version__ = "2016:01:12 12:00"


import  drivers.u6control as u6control
import drivers.wellcontrol as wellcontrol
import csv
import time
wc = wellcontrol.wcontrol()
channellist = [7,3,6,2,5,1,4,0]
port = 'COM3'
wc.connectBoard0(port)

def run(port,device,well,color,DACs):
  
  file = open('calibrationplate data/lightdata well '+ str(well) +' '+ color +'.txt', 'w')
  writer = csv.writer(file)
  line = ["DAC",color ]
  writer.writerow(line) # appends line to list.
  
  colordict = {"blue":0,"green":1,"red":2,"white":3}
  lednr = (well-1)*4
  (board,dev,chan) = wc.led_to_bdc(lednr+colordict[color])
  #measuring each value
  print "running calibration of well "+str(well)+" "+ color + " led"
  for i in DACs:
    print i # printing progress marker for every measurement
    wc.leddac(0,dev, channellist[chan], value = i)
    time.sleep(1)
    line = [i,str(raw_input("Photometer measurement: "))]
    writer.writerow(line) # appends line to list.
    wc.leddac(0, dev, channellist[chan], 0)
    time.sleep(1)


#MANUAL RUN
if __name__ == "__main__":
    device = "1"
    DACs = range(0,4097,100)
    for well in [15]:
      for color in ["green","blue"]:
        run(port,device,well,color,DACs)
