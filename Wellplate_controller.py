#!/usr/bin/env python

"""
Wellplate_controller is used to convert commands from ether the GUI or the terminal into instructions for the scripts governing the different aspects of the system.
Main functions:
Executing the calibration protocols of the leds and the sensors.
Setting led intensities and saving/loading these intensities into a ".leds"file.
Executing Od and O2 measurements
"""
__author__ = 'Bruno Favie  <bruno.favie@student.uva.nl>'
__version__ = "2015:12:16 17:00"

import wellcontroller
import ODmeasure
import O2measure
import cPickle as pickle
import serialports
#attempt to load the calibration unit.
try:
	import calibrate_leds
except:
	print "calibration unit not found"
	raw_input("Press any key to continue...")

OD = ODmeasure.ODmeasure()
O2 = O2measure.O2measure()


class control():
	"""
	class containing all functions for operating the 24-well microplate incubator.
	"""
	
	def __init__(self,device = "none"):
		"""
		Initialisation. Connects to device based on given device numbers.
		@param device: device selected for use.
		@type  device: string
		"""
		
		from serialports import devicelist # imports devices stored in serialports
		
		if device == "none": # determining if command is given from console ( lack of device number)
			check = "on" # check device untill connection is made
		else:
			check = "off" # simply check if device exists, leave check to GUI.py
			"""
			Initiation for scrip when called from console
			Will repeat untill connection is found.
			"""
		while check == "on":
			while device not in devicelist:
				device = raw_input("Select device number: ")
			self.device = device
			self.wc = wellcontroller.wellcontroller(self.device)
			if self.wc.checkconnected() == False:
						device = "none"
						print "No response from the serial ports. Check whether an invalid number has been given, the device in question is already in use or has been disconnected!"
			else:
				check = "Done"
		if check == "off":
			while device not in devicelist:
				device = raw_input("Select device number: ")
			self.device = device
			self.wc = wellcontroller.wellcontroller(self.device)

		#try loading leds.leds dictionary with led values
		try:
			self.bgrw = pickle.load( open( "devices/"+self.device+"/"+"leds.leds", "rb" ))
		except:
			# setting up dictionary containing blue,green,red and white values of each well if loading leds.leds fails
			self.bgrw = {}
			for i in range(1,49):
				self.bgrw[i] = [0,0,0,0]
		self.colordict = {"blue":0,"green":1,"red":2,"white":3}
		#listing disabled wells based on missing boards
		self.dw = self.wc.disabledwells()
	
	def calibrate_leds(self,QtGui,device):
		"""
		function responsible for executing the led calibration command given by the GUI.
		
		NOTE: if not using Gui the calibrate_leds.py file can be run on it's own for calibration!
		@param QtGui: PyQT module used for creating messageboxes
		@type  well: Module
		@param device: device selected for use.
		@type  well: string
		"""
		#loading calibration class from calibrate_leds.py
		cal = calibrate_leds.calibration(device)
		cal.set_calibrationfile()
		#Star of instructions
		#plate 1
		QtGui.QMessageBox.information(QtGui.QWidget()," Calibration instructions","Insert the calibration plate between the wellplate cover and the wellplate bed of the first plate."+'\n'+"Point the leds directed downwards towards the wellplate."+'\n'+"Make sure that the setup is secured.")
		print "Measuring..."
		cal.measurement(self.wc,"bed",plate = 0)#measuring
		#instructions
		QtGui.QMessageBox.information(QtGui.QWidget()," Calibration instructions","Insert the calibration plate between the wellplate and the wellplate bed of the first plate."+'\n'+ "Point leds directed upwards towards the wellplate."+'\n'+"Make sure that the setup is secured.")
		#raw_input("Press any key to continue...")
		print "Measuring..."
		cal.measurement(self.wc,"cover",plate = 0)#measuring
		#plate 2
		QtGui.QMessageBox.information(QtGui.QWidget()," Calibration instructions","Insert the calibration plate between the wellplate cover and the wellplate bed of the second plate."+'\n'+"Point the leds directed downwards towards the wellplate."+'\n'+"Make sure that the setup is secured.")
		print "Measuring..."
		cal.measurement(self.wc,"bed",plate = 1)#measuring
		#instructions
		QtGui.QMessageBox.information(QtGui.QWidget()," Calibration instructions","Insert the calibration plate between the wellplate and the wellplate bed of the second plate."+'\n'+ "Point leds directed upwards towards the wellplate."+'\n'+"Make sure that the setup is secured.")
		#raw_input("Press any key to continue...")
		print "Measuring..."
		cal.measurement(self.wc,"cover",plate = 1)#measuring
	
	def calibrate_sensors(self,QtGui,device):
		"""
		function responsible for executing the led calibration command given by the GUI.
		@param QtGui: PyQT module used for creating messageboxes
		@type  well: Module
		@param device: device selected for use.
		@type  well: string
		
		TO BE ADDED LATER
		"""
		print "sensorcalibration"
	
	def checkconnected(self):
		"""
		function for checking if any 20 wellplate incubator is properly connected to the computer.
		returns output from wellcontroller.py function
		"""
		return self.wc.checkconnected()
	
	
	def loadsetup(self,filename):
		"""
		loads a ".leds" file containing a dictionary of led intensity values based on the given filename.
		returns the dictonary.
		@param filename: name of the file with the led values.
		@type  filename: string
		"""
		#load setup file
		self.bgrw = pickle.load( open( filename, "rb" ))
		#save setup file as leds.leds
		pickle.dump(self.bgrw, open( "devices/"+self.device+"/"+"leds.leds", "wb" ) )
		for i in range(1,49):
			self.wc.well_restore(i,self.bgrw)
			
	def savesetup(self,filename):
		"""
		Saves the dictionary of led intensity values into a .leds file.
		@param filename: name of the file for storing the led values.
		@type  filename: string
		"""
		#save setup file under a new name
		pickle.dump( self.bgrw, open( filename, "wb" ) ) 
	

	def ledsupdate(self,color,value,wells):
		"""
		function for converting instructions for light intensities per well into direct commands for each led for each individual well.
		after updating all wells the led values are stored into "leds.leds" for easy recall of settings on other functions. 
		@param color: led color
		@type color: string
		@param value: light intensity of the led
		@type value: integer
		@param well: list containing the well numbers. standard range is 1:48
		@type  well: list or int
		"""
		#Turns single well value in list of 1 value
		if type(wells) is int:
			wells = [wells]
		#subtracting disabled wells from well list
		wells = self.wc.activewell(wells)
		if wells == -1:
			return
		for well in wells:
			#storing value in dictionary for feedback
			self.bgrw[well][self.colordict[color]] = value
			#sending value to welcontroller.py
			self.wc.led(color,value,well)
		#writing dbfile for dictionaty storage/loading.
		pickle.dump( self.bgrw, open( "devices/"+self.device+"/"+"leds.leds", "wb" ) )

	def feedback(self,wells):
		"""
		functon for returning the values of al of the leds connected to the specified wells from the bgrw dictionary.
		In case of multiple wells a comparison is made between the wells to see if the settings are homogenous. If not, an empty row [0,0,0,0] is returned to indicate thise differences.
		@param well: list containing the well numbers. standard range is 1:48
		@type  well: list or int
		"""
		if type(wells) is int:
			wells = [wells]
		#subtracting disabled wells from well list
		wells = self.wc.activewell(wells)
		# check if wel is active.
		if wells == -1:
			return [0,0,0,0] #returns 0 values for non-activated well
		# check if all wells have equal values.
		for i in wells:
			if self.bgrw[wells[0]] != self.bgrw[i]: # if all wells are not equal to the first well from the set.
				return -1 #returns 0 values for non-matching well set.
		return self.bgrw[wells[0]]
		  
	def total_shutdown(self):
		"""
		function which completely turns off all leds, including the OD leds. Used by the STOP button of the GUI.
		"""
		for w in range(1,49):
			self.wc.well_off(w)
			self.wc.OD_off(w)
	
	def read_sensors(self,wells):
		"""
		function which returns the photodiode values from the selected wells
		"""
		self.sensordict = {}
		if type(wells) is int:
			wells = [wells]
		#subtracting disabled wells from well list
		wells = self.wc.activewell(wells)
		for well in wells:
			self.sensordict[well] = self.wc.read_photodiode(well)
		return self.sensordict

	def OD_measure(self,wells):
		"""
		function which calls on ODmeasure.py for an OD-measurement of the selected wells.
		@param well: list containing the well numbers. standard range is 1:48
		@type  well: list or int
		"""
		self.sensordict = {}
		if type(wells) is int:
			wells = [wells]
		#subtracting disabled wells from well list
		wells = self.wc.activewell(wells)
		if wells == -1:
			return -1
		for well in wells:
			self.sensordict[well] = OD.OD_pulse(well,self.wc,self.device)
		return self.sensordict
	
	def O2_measure(self,wells):
		"""
		function which calls on O2measure.py for an O2 measurement of the selected wells.
		@param well: list containing the well numbers. standard range is 1:48
		@type  well: list or int
		"""
		self.sensordict = {}
		if type(wells) is int:
			wells = [wells]
		#subtracting disabled wells from well list
		wells = self.wc.activewell(wells)
		if wells == -1:
			return -1
		for well in wells:
			self.sensordict[well] = O2.O2_pulse(well,self.wc,self.device)
		return self.sensordict
	

