#!/usr/bin/env python
__author__ = 'Bruno Favie <bruno.favie@student.uva.nl>'
__version__ = "2015:12:03 16:00"
import cPickle as pickle
import os

def ledcal(device = 1):
  #check for device folder
  npath =  "devices/"+device
  if not os.path.exists(npath):
          os.makedirs(npath)
  a = 10
  b = 0
  llist = [[a,b]]*192
  pickle.dump(llist,open( "devices/"+device+"/"+'ledcalibration.cal', "wb" ))

if __name__ == '__main__':
  device = raw_input("Select device number: ")
  ledcal(device)
