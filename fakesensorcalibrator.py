#!/usr/bin/env python
__author__ = 'Bruno Favie <bruno.favie@student.uva.nl>'
__version__ = "2015:10:13 13:30"
import cPickle as pickle
import os

def sensorcal(device = 1):
  #check for device folder
  npath =  "devices/"+device
  if not os.path.exists(npath):
          os.makedirs(npath)
  a = 0.001
  b = 0
  llist = [[a,b]]*48
  pickle.dump(llist,open(  "devices/"+device+"/"+'sensorcalibration.cal', "wb" ))

if __name__ == '__main__':
  device = raw_input("Select device number: ")
  sensorcal(device)