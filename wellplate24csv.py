#!/usr/bin/env python
"""
this module comains classes that can create a folder containing different .csv files to store measurement data and
edit existing.csv files to append additional measurements.
"""
__author__ = 'Bruno Favie  <bruno.favie@student.vu.nl>'
__version__ = "2015:10:07 14:00"


import sys
import os
from datetime import datetime
from os import path, rename, remove
import csv


"""
Setting experiment name/filename and a label, used for a more detailed description, based on the given system arguments.
Returns error if more then 2 additional arguments are given.
If no arguments are given 'TEMP' is used.
"""

class csvbasics():
    """
    class containing functions shared by both the file management and file editing classes
    """
    def gettime(self):
        """
        returns date and time in string format
        """
        time = datetime.now().strftime('%Y-%m-%d %H:%M:%S') # stores tim in string.
        return time

    def checkforfolder(self,experiment):
        """
        checks whether the experiment's unique folder is found. If not found creates the required folder
        """
        npath = "results/"+experiment
        if not os.path.exists(npath):
          os.makedirs(npath)

    def checkforfile(self,experiment,mtype):
        """
        checks whether a .csv file is found in the experiment's unique folder.
        Returns ether True(1) or False(0)
        """
        try:
            file = open("results/"+experiment+"/"+mtype+'.csv', 'r')
            return True
        except:
            return False

def csvindependant():
    """
    Uses system arguments to set experiment name and label when called from command line.
    """
    label = None # set label as empty.
    if len(sys.argv) >3: # checks whether no many arguments are given
        print "to many arguments! correct format is ['wellplate24csv.py' 'experiment' 'optional description of experiment']"
        exit()
    if len(sys.argv) == 1: # if no additional arguments are given.
        experiment = "TEMP"
    if len(sys.argv) >1: #checks if additional system argumens are given
        experiment = sys.argv[1] # sets first additional system argument as the filename
    if len(sys.argv) == 3: #checks 2 additional system arguments are given
        label = (sys.argv[2]) # Uses the second additional system argument as the label in the csv file.
    return (experiment,label)



class csvfilemanagement(csvbasics):
    """
    class responsible for checking for existing csv files for current experiment.
    loading data from an existing file or creating a new file if no file is found.
    """
    def __init__(self):
        pass

    
    def createcsv(self,experiment,mtype,label = None):
        """
        Creates an csv file for both OD and O2 measurement. Will return TRUE if an existing file with that name was already found.
        Adds header, starting time and label to first row.
        Adds time + wellnames to 3rd row.
        @param experiment: Name of Experiment. Used as the filename for storing the measurements.
        @type experiment: string
        @param label: Optional label for csv file. Can contain additional info.
        @type label: string
        """
        self.checkforfolder(experiment)
        if self.checkforfile(experiment,mtype) is True:
            return -1 # file found.
        file = open("results/"+experiment+"/"+mtype+'.csv', 'ab')
        writer = csv.writer(file)
        writer.writerow(('Project Name:',experiment,'Date:',self.gettime(),'','',label))
        writer.writerow("  ")
        row  = ['time','well 1', 'well 2', 'well 3', 'well 4', 'well 5', 'well 6', 'well 7', 'well 8', 'well 9', 'well 10',
                          'well 11', 'well 12', 'well 13', 'well 14', 'well 15', 'well 16', 'well 17', 'well 18', 'well 19', 'well 20',
                           'well 21', 'well 22', 'well 23', 'well 24', 'well 25', 'well 26', 'well 27', 'well 28', 'well 29', 'well 30',
                            'well 31', 'well 32', 'well 33', 'well 34', 'well 35', 'well 36', 'well 37', 'well 38', 'well 39', 'well 40',
                             'well 41', 'well 42', 'well 43', 'well 44', 'well 45', 'well 46', 'well 47', 'well 48']
        writer.writerow(row)
        file.close()


class csvfileedit(csvbasics):
    """
    class responsible for adding new measurement values to a csv file of the current project
    """


    def __init__(self):
        pass
    
    def appendrow(self,experiment,mtype,row):
        """
        appends latest measurements as a new row to csv file
        @param experiment: Name of Experiment/ name of file for storing the measurements.
        @type experiment: string
        @param mtype: type of measurement
        @type mtype: string
        @param row: list of timeframe and sensorvalues.
        @type row: list
        """
        if self.checkforfile(experiment,mtype) is False:
            csvfilemanagement().createcsv(experiment,mtype)
        file = open("results/"+experiment+"/"+mtype+'.csv', 'ab')
        writer = csv.writer(file)
        line = [self.gettime()]# creates a list 'line' with the timeframe as first value
        line.extend(row) # extends the list with all the sensor values.
        writer.writerow(line) # appends line to list.
        
if __name__ == '__main__':
    """
    creates a new folder for the experiment, along with two .csv files. for OD and O2 measurement values.
    """
    (experiment,label) = csvindependant()
    c = csvfilemanagement()
    c.createcsv(experiment,"OD",label)
    c.createcsv(experiment,"O2",label)