"""
GUI.py provides an graphical interface for controlling the 24 well microplate incubator.
most of the interface functions are provided by callong onto the wellplate_controller.py script.
The interface itself uses the GUI.ui file for the visual elements.
"""
__author__ = 'Bruno Favie <bruno.favie@student.uva.nl>'
__version__ = "2015:12:08 16:00"



import sys
import os.path
from os import system
from PyQt4.uic import loadUiType
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import Qt, QTimer
from PyQt4.QtGui import QApplication, QCursor
from functools import partial
import wellplate_controller
import wellplate24csv
import serialports
#loading GUI from .ui file.
Ui_MainWindow, QMainWindow = loadUiType('GUI.ui')
class Main(QMainWindow, Ui_MainWindow):
		"""
		Class containing all functions related to the main window.
		Initiates the the main window by loading the GUI.ui file and connects the signals of the buttons with the functions defined in this class.
		"""
		def __init__(self, ):
			super(Main, self).__init__()
			self.setupUi(self)
			
			"""
			Initialisation function. Attempts to connect to one of the the 24 well microplate incubator and will wait until a connection is made.
			connects buttons to other functions in this class and sets internal countdowns for running measurement batches.
			"""
			finalcheck = False # loop variable. Will keep looping untill a working connection is made.
			self.device = 0 # defining device variable
			from serialports import devicelist
			while finalcheck == False:
				while self.device not in devicelist: #LIST OF POSSIBLE DEVICES RECOGNISED BY THE GUI! CHANGE THIS IF ADDING MORE DEVICES
					device, ok = QtGui.QInputDialog.getText(self, 'Device','Enter the device number here:')
					if ok:
						self.device = str(device)
					else:
						# if cancel is pressed, program is closed.
						sys.exit()
				# setting console window title  based on selected device
				if sys.platform == "win32": # Program is ron on windows
					system("title "+"24 Well microplate incubator Device #"+self.device )
				elif sys.platform == "linux" or sys.platform == "linux2": # Program is run on linux
					sys.stdout.write("\x1b]2;"+"24 Well microplate incubator Device #"+self.device+"\x07")
				#setting UI device number on interface
				self.devicenumber.setText(self.device)
				#initiating wellplate_controller class from wellplate_controller.py
				self.wellcontrol = wellplate_controller.control(self.device)
				#check whether any devices are actually connected.Breaks loop if connection is established
				if self.wellcontrol.checkconnected() == True:
					finalcheck = True
				else:
					QtGui.QMessageBox.information(QtGui.QWidget(),"Device not found!","No response from the serial ports. Check whether an invalid number has been given, the device in question is already in use of is disconnected!")
					self.device = 0 #resetting device variable

			#signals and slots from buttons. Connects button presses to functions listed further below
			self.white_off.clicked.connect(self.whiteoff)
			self.red_off.clicked.connect(self.redoff)
			self.green_off.clicked.connect(self.greenoff)
			self.blue_off.clicked.connect(self.blueoff)
			self.OFFbutton.clicked.connect(self.ALLoff)
			self.ODbutton.clicked.connect(self.OD)
			self.O2button.clicked.connect(self.O2)
			self.startODbutton.clicked.connect(self.startOD)
			self.startO2button.clicked.connect(self.startO2)
			self.sensorbutton.clicked.connect(self.sensorupdate)
			
			#signal from menu
			self.actionLoad_setup.triggered.connect(self.loadsetup)
			self.actionSave_setup.triggered.connect(self.savesetup)
			self.actionNew_Experiment.triggered.connect(self.newexperiment)
			self.actionLoad_Experiment.triggered.connect(self.loadexperiment)
			self.actionCalibrate_leds.triggered.connect(self.calibrateleds)
			self.actionCalibrate_sensors.triggered.connect(self.calibratesensors)
			
			#signals from single well selection
			self.wellspin.valueChanged.connect(self.wellboxreset)
			#signal from selecting a wellseries
			self.wellBox.currentIndexChanged.connect(self.wellselection)
			
			#signalling update of led values
			#spinbox signals
			self.Redspin.valueChanged.connect( partial( self.ledupdate, 'red'))
			self.Bluespin.valueChanged.connect( partial( self.ledupdate, 'blue'))
			self.Greenspin.valueChanged.connect( partial( self.ledupdate, 'green'))
			self.Whitespin.valueChanged.connect( partial( self.ledupdate, 'white'))
			#slider signals
			self.redledslider.sliderReleased.connect(self.redslider)
			self.greenledslider.sliderReleased.connect(self.greenslider)
			self.blueledslider.sliderReleased.connect(self.blueslider)
			self.whiteledslider.sliderReleased.connect(self.whiteslider)
			
			#setting up list containing lists of wells
			self.wellgroup = [1,range(1,49),range(1,25),range(25,49),range(1,25,4),range(2,25,4),range(3,25,4),range(4,25,4),
									  range(25,49,4),range(26,49,4),range(27,49,4),range(28,49,4),range(1,5),range(5,9),range(9,13),range(13,17),range(17,21),range(21,25),
									  range(25,29),range(29,33),range(33,37),range(37,41),range(41,45),range(45,49)]
			
			#creating list to store current selection of wells.
			# devault value == all wells selectedStarts with all wells selected.
			self.wells = range(1,49)
			
			#setting spinbox index to "all wells selected"
			self.wellBox.setCurrentIndex(1)
			
			#setting default experiment name to "TEMP"
			self.experiment = "TEMP"
			
			#Timers used for repeated measurements:
			#OD
			self.ODtimer = QTimer()
			self.ODtimer.timeout.connect(self.startOD)
			self.ODqueu = 0 # defining queu value for OD measurement batches
			
			#O2
			self.O2timer = QTimer()
			self.O2timer.timeout.connect(self.startO2)
			self.O2queu = 0 # defining queu value for O2 measurement batches
			
			#refresing the sliders to show the current light values.
			self.wellBox.setCurrentIndex(0)
			self.wellBox.setCurrentIndex(1)
		def redslider(self):
			"""
			function which updates the red spinbox with the last slider value
			"""
			self.Redspin.setValue(self.redledslider.value())
			
		def blueslider(self):
			"""
			function which updates the blue spinbox with the last slider value
			"""
			self.Bluespin.setValue(self.blueledslider.value())
		
		def greenslider(self):
			"""
			function which updates the green spinbox with the last slider value
			"""
			self.Greenspin.setValue(self.greenledslider.value())
		
		def whiteslider(self):
			"""
			function which updates the white spinbox with the last slider value
			"""
			self.Whitespin.setValue(self.whiteledslider.value())
		
		
		
		def sliderupdate(self,wells):
			"""
			function which updates the interface sliders based on the selected wells.
			@param well: list containing the well numbers. standard range is 1:48
			@type  well: list or int
			"""
			QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
			bgrw = self.wellcontrol.feedback(wells)
			if bgrw == -1 and len(self.wellgroup) > 1 : # Checks if multiple wels with varying led values are selected.
				QtGui.QMessageBox.information(QtGui.QWidget()," Take note! ",
"Not all of the wells in this selection share the same led settings. Adjusting the led intensities for a specific color will overwrite all previous settings for that color for these wells.")
				bgrw = [0,0,0,0]
				self.whiteledslider.setSliderPosition(bgrw[3])
				self.redledslider.setSliderPosition(bgrw[2])
				self.greenledslider.setSliderPosition(bgrw[1])
				self.blueledslider.setSliderPosition(bgrw[0])
				
				self.Redspin.blockSignals(True)
				self.Redspin.setValue(self.redledslider.value())
				self.Redspin.blockSignals(False)
				self.Bluespin.blockSignals(True)
				self.Bluespin.setValue(self.blueledslider.value())
				self.Bluespin.blockSignals(False)
				self.Greenspin.blockSignals(True)
				self.Greenspin.setValue(self.greenledslider.value())
				self.Greenspin.blockSignals(False)
				self.Whitespin.blockSignals(True)
				self.Whitespin.setValue(self.whiteledslider.value())
				self.Whitespin.blockSignals(False)
				QApplication.restoreOverrideCursor()
			else:
				self.whiteledslider.setSliderPosition(bgrw[3])
				self.redledslider.setSliderPosition(bgrw[2])
				self.greenledslider.setSliderPosition(bgrw[1])
				self.blueledslider.setSliderPosition(bgrw[0])
				self.redslider()
				self.blueslider()
				self.whiteslider()
				self.greenslider()
				QApplication.restoreOverrideCursor()
		
		def wellboxreset(self):
				"""
				function which sets the wellbox index to 0, which repesents selecting a single well
				The well selected is determined by the spinboxvalue from "wellspin".
				Updates the sliders after selecting the wells.
				"""
				QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
				self.wellgroup[0] = self.wellspin.value()
				self.wells = self.wellgroup[0]
				self.wellBox.setCurrentIndex(0)
				self.sliderupdate(self.wells)
				QApplication.restoreOverrideCursor()
				
		def wellselection(self):
				"""
				function which selects the wells based on wellbox selection.
				Update all sliders with the values from the wels by taking the values of the first well.
				Updates the sliders after selecting the wells.
				"""
				QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
				i = self.wellBox.currentIndex()
				self.wells = self.wellgroup[i]
				#update sliders based on stored data
				self.sliderupdate(self.wells)
				QApplication.restoreOverrideCursor()
				
		def OD(self):
				"""
				function which executes the OD measurement function for each active well selected by the GUI
				Updates the sensor values on the GUI
				"""
				QApplication.setOverrideCursor(QCursor(Qt.WaitCursor)) # locks screen
				sensordict = self.wellcontrol.OD_measure(self.wells) # calls OD measurement function
				#updating screen with new sensor values
				if sensordict != -1: # checks if measurements were preformed
						for w in sensordict:
								exec('main.lcdwell_'+ str(w)+'.setProperty("value", sensordict['+str(w)+'])')
						self.storecsv(sensordict,"OD")
				QApplication.restoreOverrideCursor() # unlock screen
				
		def O2(self):
			"""
			function which executes the O2 measurement function for each active well selected by the GUI
			Updates the sensor values on the GUI
			"""
			QApplication.setOverrideCursor(QCursor(Qt.WaitCursor)) # locks screen
			sensordict = self.wellcontrol.O2_measure(self.wells) # calls O2 measurement function
			#updating screen with new sensor values
			if sensordict != -1:# checks if measurements were preformed
					for w in sensordict:
							exec('main.lcdwell_'+ str(w)+'.setProperty("value", sensordict['+str(w)+'])')
					self.storecsv(sensordict,"O2")
			QApplication.restoreOverrideCursor()#unlocks screen
				
		def startOD(self):
			"""
			function which ececutes a batch of OD measurements with a set interval between measurements.
			"""
			# fcheck if the given number of measurements is smaller then 1. Function will not continue if true 
			if self.numberofOD.value() < 1:
				return
			#set status label
			self.ODstate.setText("measurements in progress")
			#first run actions
			if self.ODqueu == 0:
				# pop-up notification
				QtGui.QMessageBox.information(QtGui.QWidget(),"OD","Starting measurement")
				#All menu element regarding the meausrement are locked to prevent them from being overwritten.
				self.hoursOD.setReadOnly(True)
				self.minutesOD.setReadOnly(True)
				self.numberofOD.setReadOnly(True)
				self.startODbutton.blockSignals(True)
				#calculate interval
				self.ODinterval = (60 * self.hoursOD.value() + self.minutesOD.value())*60000
				# set queu
				self.ODqueu = self.numberofOD.value()
				#set progressbar
				self.progressOD.setMaximum(self.ODqueu)
				self.runOD = 0
				self.progressOD.setValue(self.runOD)
				#start timer
				self.ODtimer.start(self.ODinterval)
			#reset timer
			self.ODtimer.start(self.ODinterval)
			# Measurement
			self.OD()
			
			#calculate remaining number of measurements
			self.ODqueu = self.ODqueu - 1
			#update progress bar
			self.runOD = self.runOD + 1
			self.progressOD.setValue(self.runOD)
			#check if last measurement has already been completed. If so, unlocks the menu and stops the timer.
			if self.ODqueu == 0:
				#stop the timer
				self.ODtimer.stop()
				#unlock menu
				self.hoursOD.setReadOnly(False)
				self.minutesOD.setReadOnly(False)
				self.numberofOD.setReadOnly(False)
				self.startODbutton.blockSignals(False)
				#set status label
				self.ODstate.setText("measurements completed")
		
		def startO2(self):
			#abort if repeats is 0 or negative
			if self.numberofO2.value() < 1:
				return
			#set status label
			self.ODstate.setText("measurements in progress")
			if self.O2queu == 0:
				# pop-up notification
				QtGui.QMessageBox.information(QtGui.QWidget(),"O2","Starting measurement")
				#All menu element regarding the meausrement are locked to prevent them from being overwritten.
				self.hoursO2.setReadOnly(True)
				self.minutesO2.setReadOnly(True)
				self.numberofO2.setReadOnly(True)
				self.startO2button.blockSignals(True)
				#calculate interval
				self.O2interval = (60 * self.hoursO2.value() + self.minutesO2.value())*60000
				# set queu
				self.O2queu = self.numberofO2.value()
				#set progressbar
				self.progressO2.setMaximum(self.O2queu)
				self.runO2 = 0
				self.progressO2.setValue(self.runO2)
				#start the timer
				self.O2timer.start(self.O2interval)
			#reset the timer
			self.O2timer.start(self.O2interval)
			# Measurement
			self.O2()
			
			#calculate remaining number of measurements	
			self.O2queu = self.O2queu - 1
			#update progress bar
			self.runO2 = self.runO2 + 1
			self.progressO2.setValue(self.runO2)
			#check if last measurement has already been completed. If so, unlocks the menu and stops the timer.
			if self.O2queu == 0:
				#stop the timer
				self.O2timer.stop()
				#unlock menu
				self.hoursO2.setReadOnly(False)
				self.minutesO2.setReadOnly(False)
				self.numberofO2.setReadOnly(False)
				self.startO2button.blockSignals(False)
				self.ODstate.setText("measurements completed")
				print "measurements completed"
				
				
		#STOP Buttons. Resetting led intensity to 0 when triggered.
		def whiteoff(self):
			"""
			sets the light intensity of the white leds from the selected wells to 0
			"""
			self.whiteledslider.setSliderPosition(0)
			self.whiteslider()
			self.ledupdate('white')
		def redoff(self):
			"""
			sets the light intensity of the red leds from the selected wells to 0
			"""
			self.redledslider.setSliderPosition(0)
			self.redslider()
			self.ledupdate('red')
		def greenoff(self):
			"""
			sets the light intensity of the green leds from the selected wells to 0
			"""
			self.greenledslider.setSliderPosition(0)
			self.greenslider()
			self.ledupdate('green')
		def blueoff(self):
			"""
			sets the light intensity of the blue leds from the selected wells to 0
			"""
			self.blueledslider.setSliderPosition(0)
			self.blueslider()
			self.ledupdate('blue')
		
			
		def ALLoff(self):
			"""
			Forcefully turns off all leds, active or inactive, and resets the well selection box.
			"""
			QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))#locks window
			self.wellcontrol.total_shutdown()
			self.wellBox.setCurrentIndex(0)
			self.wellBox.setCurrentIndex(1)
			QApplication.restoreOverrideCursor()#unlocks window
				
		def loadsetup(self):
			"""
			function for selecting a  ".leds" file containing stored led intensity settings. Function used for loading stored LED setups.
			"""
			filename = QtGui.QFileDialog.getOpenFileName(main,'Open File',"")
			QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))
			self.wellcontrol.loadsetup(filename)
			self.wellBox.setCurrentIndex(0)
			self.wellBox.setCurrentIndex(1)
			QApplication.restoreOverrideCursor()

		def savesetup(self):
			"""
			function for saving current led setup into a ".led" file for later use
			"""
			filename = QtGui.QFileDialog.getSaveFileName(main, "Save file", "",".leds")
			self.wellcontrol.savesetup(filename)
		
		# def set_t0(self):
		# 	"""
		# 	stores current led setting as a starting point. Used to adjust OD values.
		# 	"""
		# 	self.wellcontrol.set_t0()
		def loadexperiment(self):
			"""
			function for loading an existing experriment by selecting a folder
			"""
			experiment =  QtGui.QFileDialog.getExistingDirectory(main,'Load Experiment')
			self.experiment =  os.path.splitext(os.path.basename(str(experiment)))[0]
			self.experimentname.setText(self.experiment)
		
		def newexperiment(self):
			"""
			function for creating a new experiment folder, containing an empty OD and O2 measurement file.
			"""
			#Entering experiment name trough text input box
			experiment, ok = QtGui.QInputDialog.getText(self, 'New Experiment', 
			'Enter experiment name:')
			if ok:
				self.experiment = str(experiment)
			#check if folder already exists
			npath = "results/"+experiment
			if os.path.exists(npath):
				QtGui.QMessageBox.information(QtGui.QWidget()," Take note! ","Folder already exists!")
				return
			self.experimentname.setText(self.experiment)
			#adding an optional description to the experiment
			text, ok = QtGui.QInputDialog.getText(self, 'Input Dialog', 'Add an description for this experiment:')
			if ok:
					label= str(text)
			csv = wellplate24csv.csvfilemanagement()
			for mtype in ["OD","O2"]:
				csv.createcsv(self.experiment,mtype,label = label)
				
				
		def ledupdate(self,color):
			"""
			function for regulating led intensities of the hardware of all selected wells.
			@param color: led color
			@type color: string
			"""
			QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))#lock windows
			# Add value,wells
			valuedict = {'red':self.Redspin.value(),"blue":self.Bluespin.value(),'green':self.Greenspin.value(),'white':self.Whitespin.value()}
			#send color, value and wells to wellplate controller.
			self.wellcontrol.ledsupdate(color,valuedict[color],self.wells)
			QApplication.restoreOverrideCursor()#unlock window

		def sensorupdate(self):
			"""
			function for updating the displayed sensor values of the GUI
			"""
			sensordict = self.wellcontrol.read_sensors(self.wells)# load values
			for w in sensordict:
					exec('main.lcdwell_'+ str(w)+'.setProperty("value", sensordict['+str(w)+'])') # update window

		def storecsv(self,sensordict,mtype):
			"""
			function for storing measurement data into a csv file
			@param sensordict: dictionary with sensor values
			@type sensordict: dictionary
			@param mtype: measurement type
			@type mtype: string
			"""
			csv = wellplate24csv.csvfileedit()
			measurements = [None]*48
			for w in sensordict:
					measurements[w-1] = sensordict[w]
			csv.appendrow(self.experiment,mtype, measurements)

		def calibrateleds(self):
			"""
			function for calling on the led calibration function of welplate_controller.py
			"""
			self.wellcontrol.calibrate_leds(self.device,QtGui)
				
		def calibratesensors(self):
			"""
			function for calling on the sensor calibration function of welplate_controller.py
			"""
			self.wellcontrol.calibrate_sensors(self.device,QtGui,)

		def check_calibrationfiles(self):
			"""
			function for checking if all calibration files for the device are found, prompting a warning if one or more files are missing.
			"""
			try:
				#loading calibration file
				open("devices/"+self.device+"/"+'ledcalibration.cal', "rb" )
			except IOError: 
				QtGui.QMessageBox.critical(QtGui.QWidget()," Take note! ","Led calibration file not found! Please preform the led calibration before attempting to run a test!")
			try:
				#loading Sensor calibration file
				open("devices/"+self.device+"/"+'sensorcalibration.cal', "rb" )
			except IOError: 
				QtGui.QMessageBox.critical(QtGui.QWidget()," Take note! ","Sensor calibration file not found! Please preform the sensor calibration before attempting to run a test!")
		def windowtitle(self):
			"""
			function used to update the GUI window title with the device number after selecting a device.
			"""
			return "24 Well microplate incubator Device #"+self.device
		
# main function
if __name__ == '__main__':
		app = QtGui.QApplication(sys.argv)
		main = Main()
		main.show()
		# setting Ui window title
		main.setWindowTitle(main.windowtitle())
		main.check_calibrationfiles()
		sys.exit(app.exec_())
