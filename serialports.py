#!/usr/bin/env python
"""
serialports.py is used to determine which serial ports are to be assigned to each 24 well microplate incubator.
As each operating system has a different format the script determins which OS is used before assigning the correct serial ports.
Because each device has it's own set of serial ports multyiple devices can work simultaniously, even when using the GUI.
"""
__author__ = 'Bruno Favie  <bruno.favie@student.uva.nl>'
__version__ = "2015:12:08 16:00"
"""
TAKE NOTE! ADJUSTING THESE VALUES WILL DETERMINE WHICH SERIAL PORTS ARE USED BY ALL SCRIPTS!
REGULAR USE OF THE SYSTEM DOES NOT REQUIRE THESE VALUES TO BE CHANGES UNLESS THE HARDWARE HAS BEEN DISCONNECTED OR RECONNECTED TO A DIFFERENT SYSTEM!
ONLY CHANGE THESE VALUES IF YOU KNOW WHAT YOU ARE DOING!
"""
from sys import platform

#Device list: This list is the masterlist used by both GUI.py and wellplate_contr used by all other scripts to determine which devices are available.
#NOTE: The entries in this list need to be entered as strings ("#")
devicelist = ["1","2","3"]
def serialports(device):
  if device == "1":
    "connects comports. parameter device is used to allow multiple units to run at the same time."
    if platform == "linux" or platform == "linux2":
      (COM0,COM1,COM2) = ("/dev/ttyACM0","/dev/ttyACM1","/dev/ttyACM2")
    elif platform == "darwin":
        (COM0,COM1,COM2) = ("COM3","COM4","COM5")
    elif platform == "win32":
        (COM0,COM1,COM2) = ("COM3","COM4","COM5")
    return (COM0,COM1,COM2)
  
  if device == "2":
    "connects comports. parameter device is used to allow multiple units to run at the same time."
    if platform == "linux" or platform == "linux2":
      (COM0,COM1,COM2) = ("/dev/ttyACM3","/dev/ttyACM4","/dev/ttyACM5")
    elif platform == "darwin":
        (COM0,COM1,COM2) = ("COM6","COM7","COM8")
    elif platform == "win32":
        (COM0,COM1,COM2) = ("COM6","COM7","COM8")
    return (COM0,COM1,COM2)
  
  if device == "3":
    "connects comports. parameter device is used to allow multiple units to run at the same time."
    if platform == "linux" or platform == "linux2":
      (COM0,COM1,COM2) = ("/dev/ttyACM6","/dev/ttyACM7","/dev/ttyACM8")
    elif platform == "darwin":
        (COM0,COM1,COM2) = ("COM9","COM10","COM11")
    elif platform == "win32":
        (COM0,COM1,COM2) = ("COM9","COM10","COM11")
    return (COM0,COM1,COM2)
    