#!/usr/bin/env python
"""
Welcontroller.py contains the most basic commands for the 24 well microplate incubator and is used to establish the connections at the start of each session.
functions in this script are based on the controlling the elements connected to a  single well.
"""
__author__ = 'Bruno Favie  <bruno.favie@student.uva.nl>'
__version__ = "2015:12:16 17:00"

import time
import drivers.wellcontrol as wellcontrol
import serialports as sp
import cPickle as pickle
import math

class wellcontroller():
	"""
	class containing all the functions for controlling elements of a single well.
	"""
	
	def __init__(self,device):
		"""
		Initialisation. loads serial ports and establishes a connection with the incubator hardware. 
		Defines lists containing unit numbers of Od leds and sensors for converting well numbers to unit numbers
		param device: device selected for use.
		@type  well: string
		"""
		self.device = device
		#setting comports depending on system.
		(COM0,COM1,COM2) = sp.serialports(self.device)
		#connecting to measurement boards.
		self.wc = wellcontrol.wcontrol()
		self.openconnections(COM0,COM1,COM2)
		self.colordict = {"blue":0,"green":1,"red":2,"white":3}
		# welllist is used to connect each wellnumber to the first led illuminating said well.
		self.welllist = [0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 64,68, 72, 76, 80, 84, 88, 92, 96, 100, 104, 108, 112, 116, 120, 124, 128,132, 136, 140, 144, 148, 152, 156, 160, 164, 168, 172, 176, 180, 184, 188]
		#OD list containst the unit numbers of the OD leds
		self.ODlist = [14,10,15,11,8,12,9,13,6,5,7,4,0,3,1,2,30, 26, 31, 27, 24, 28, 25, 29, 22, 21, 23, 20, 16, 19, 17, 18,46, 42, 47, 43, 40, 44, 41, 45, 38, 37, 39, 36, 32, 35, 33, 34]
		#sensorlist contains the unit numbers of the photodiodes
		self.Sensorlist = [12,11,13,10,14,9,15,8,4,3,5,2,6,1,7,0,28, 27, 29, 26, 30, 25, 31, 24, 20, 19, 21, 18, 22, 17, 23, 16,44, 43, 45, 42, 46, 41, 47, 40, 36, 35, 37, 34, 38, 33, 39, 32]
		#channellist contains the unit numbers of the 8 leds connected to each 'device arranged in such a maner that they form 2 sets of 4 leds containing all of the 4 colors for each well.
		self.channellist = [7,3,6,2,5,1,4,0]
		

		#Attempt to load the calibration files.Return a  warning if one or more files are not found.
		try:
			#load Led calibration file
			self.load_ledcalibration()
		except IOError: 
			print "Led calibration file not found! Please preform the Led calibration before attempting to run a test!"
		try:
			#load Sensor calibration file
			self.load_sensorcalibration()
		except IOError: 
			print "Sensor calibration file not found! Please preform the Sensor calibration before attempting to run a test!"



	def openconnections(self,COM0,COM1,COM2):
		"""
		function used for (re)connecting to the 24 well microplate incubator
		param device: device selected for use.
		@param COM0: First serial port controlling wells 1 trough 16
		@type  COM0: string
		@param COM1: Second serial port controlling wells 17 trough 32
		@type  COM1: string
		@param COM2: Third serial port controlling wells 33 trough 48
		@type  COM2: string
		"""
		self.wc.connectBoard0(COM0)
		self.wc.connectBoard1(COM1)
		self.wc.connectBoard2(COM2)
		
	
	def checkconnected(self):
		"""check whether any of the 3 serial posts selected are actually connected"""
		if self.wc.checkConnection(0) == True:
			return True
		elif self.wc.checkConnection(1) == True:
			return True
		elif self.wc.checkConnection(2) == True:
			return True
		else:
			return False
			
	def load_ledcalibration(self):
		"""
		Function for loading the LED calibrationfile.
		"""
		self.ledcalibration = pickle.load(open("devices/"+str(self.device)+"/"+'ledcalibration.cal', "rb" ))
			
			
	def load_sensorcalibration(self):
		"""
		Function for loading the Sensor calibrationfile.
		"""
		self.sensorcalibration = pickle.load(open("devices/"+str(self.device)+"/"+'sensorcalibration.cal', "rb" ))

	def disabledwells(self):
		"""
		function which determines which wells are disabled, based on serial ports which are actually connected.
		"""
		
		#listing disabled wells for board 1 (1..16), board 2 (17..32) and board 3 (33..48)
		dw= []
		if self.wc.checkConnection(0) == False:
			dw.extend(range(1,17))
		if self.wc.checkConnection(1) == False:
			dw.extend(range(17,33))
		if self.wc.checkConnection(2) == False:
			dw.extend(range(33,49))
		return dw
	
	def activewell(self,wells):
		"""
		Function which subtracts the  disabled wells from the well selection.
		@param well: list containing the selected well numbers.
		@type  well: list or int
		"""
		dw = self.disabledwells()
		#subtracting disabled wells from well list
		wells = list(set(wells) - set(dw))
		#check whether there are any wells left in well list.
		if wells == []:
			return -1
		return wells


	#     "blue":,"green","red","white"   0..10000, 1..48
	def led(self,color,value,well):
		"""
		Activates a single led connected to the wells based on color and intensity value.
		Requires a wellnumber to select correct range of leds.
		Used OD measurement results to adjust light intensity to compensate for increases in population.
		@param color: color of the led
		@type color: string
		@param value: light intensity of the led
		@type value: inteWger
		@param wellnumbers: selected well
		@type wellnumbers: integer or list of integers
		"""
		# load OD measurement value of well
		
		if well not in range(1,49):
			return "well number out of range. Well numbers are limited to 1--48"
		lednr = self.welllist[well-1] #selecting the first led in the series of leds connected to the presented well
		(board,dev,chan) = self.wc.led_to_bdc(lednr+self.colordict[color]) # determines board, device and channel.
		#converting value using calibration file.
		"Converting mE/m2*s imput to a DAC value"
		#loading calibration values
		a =  self.ledcalibration[(well-1 )*4 + self.colordict[color]][0] #scaling
		b =  self.ledcalibration[(well-1 )*4 + self.colordict[color]][1] #offset
		if value != 0: # if light value is not 0
			value = int(round(a*value + b))
		return self.wc.leddac(board,dev,self.channellist[chan],value) # commands led driver to turn on a single led. Returns 0 or 1 
	
	
	#                   1..48          
	def well_off(self,well):
		"""
		turns off all leds connected to a specific well.
		@param wellnumbers: selected well
		@type wellnumbers: integer or list of integers
		"""
		if well not in range(1,49):
				return "well number out of range. Well numbers are limited to 1--48"
		lednr = self.welllist[well-1] #selecting the firt led in the range connected to the presented well
		(board,dev,chan) = self.wc.led_to_bdc(lednr) # determines board, device and channel.
		return[self.wc.leddac( board,dev,self.channellist[c],0) for c in range(chan,chan+4)] # commands led driver to turn off all leds

	def well_restore(self,well,bgrw):
		"""
		function which restores led values based on stored values from ledsupdate.
		Used after running functions which disable led setings.
		"""
		if well not in range(1,49):
				return "well number out of range. Well numbers are limited to 1--48"
		lednr = self.welllist[well-1] #selecting the firt led in the range connected to the presented well
		(board,dev,chan) = self.wc.led_to_bdc(lednr) # determines board, device and channel.
		for i in range(4):
				self.wc.leddac( board,dev,self.channellist[chan+i],bgrw[well][i])
				
				
		#                   1..48  
	def read_photodiode(self,wellnumber):
		"""
		function which returns the light intensity measured by the photodiode of the specific well.
		Uses calibration data for converting the reading into mE/m2*s
		@param wellnumber: selected well
		@type wellnumber: integer
		"""
		if wellnumber not in range(1,49):
				return "well number out of range. Well numbers are limited to 1--48"
		sensornr = self.Sensorlist[wellnumber-1] #selecting the sensor connected to the well
		(board, adc, diode) = self.wc.sensor_to_bad(sensornr)
		if self.wc.sensor(board, adc, diode) is not -1:
				#convering signal to mE/m2*s
				a = self.sensorcalibration[wellnumber-1][0]
				b = self.sensorcalibration[wellnumber-1][1]
				measurement = a*(self.wc.sensor(board, adc, diode))+b
				return measurement

		#          1..48
		
	def OD_on(self,well):
		"""
		turns OD led on
		"""
		if well not in range(1,49):
				return "well number out of range. Well numbers are limited to 1--48"        
		#select ledId for leddac
		(board,dev,chan) =self.wc.ODled_to_bdc(self.ODlist[well-1])
		#select ledid for led_ext
		(boardOD,deviceOD) = self.wc.ODled_to_bd(self.ODlist[well-1])
		#setting OD led value
		self.wc.leddac(board,dev,chan, value=2048)
		#Actiating OD led
		return self.wc.ledext(boardOD,deviceOD,onoff=1)
		
		#          1..48
	def OD_off(self,well):
		"""
		turns OD led off
		"""
		if well not in range(1,49):
				return "well number out of range. Well numbers are limited to 1--48"        
		#select ledId for leddac
		(board,dev,chan) =self.wc.ODled_to_bdc(self.ODlist[well-1])
		#select ledid for led_ext
		(boardOD,deviceOD) = self.wc.ODled_to_bd(self.ODlist[well-1])
		#setting OD led value
		self.wc.leddac(board,dev,chan, value=0)
		#Deacivating OD led
		return self.wc.ledext(boardOD,deviceOD,onoff=0)
	
		#          1..48
	def relax(self,well):
		"""
		turns of The blue led and starts a relaxation time measurement
		"""
		sensornr = self.Sensorlist[wellnumber-1] #selecting the sensor connected to the well
		(board, adc, diode) = self.wc.sensor_to_bad(sensornr)
		self.led(self,"blue",0,well) # turning off led
		time = self.wc.relax(self,board, adc, diode)
		return time