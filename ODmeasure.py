#!/usr/bin/env python
"""
ODmeasure.py contains all the functions required for performing an OD measurement using the 24 well microplate incubator.
It can be called upon by other scripts but can also be executed by itself, allowing for quick measurements trough a console window.
When executed on itself it will store it's data based on the first argument given, which is used as the experiment name.
The second argument used is the number of the device.

Example:

Python ODmeasure.py experiment_name #device
"""

__author__ = 'Bruno Favie  <bruno.favie@student.uva.nl>'
__version__ = "2015:12:08 16:00"



import sys
import time
import wellcontroller
import wellplate24csv
import cPickle as pickle

#setting experiment name. If no name given "TEMP" is used.
if len(sys.argv) >1:
	experiment = (sys.argv[1])
else:
	experiment = "TEMP"
class ODmeasure():
	"""
	This class contains functions which interact directly with the OD leds and the wellcontroller.
	When executed as the main script it will preform an OD measurement of each well and store the data.
	"""
	
	
	def __init__(self):
		pass
		

	def OD_pulse(self,wellnumber,wc,device):
		"""
		turns off al leds connected to a specific well before pulsing the OD-led for a OD measurement.
		Returns the sensor value
		loads the led values from the pickle file and restores illumination.
		@param wellnumber: selected well
		@type wellnumber: integer
		@param device: device number
		@type string
		"""
		self.device = str(device)
		wc.well_off(wellnumber)
		time.sleep(0.001) # time for allowing the sensor to reset
		wc.OD_on(wellnumber)
		#resting period
		time.sleep(1)
		#measurement
		measurement = wc.read_sensor(wellnumber)
		#Turning off the OD led
		wc.OD_off(wellnumber)               
		#restore well illumination
		bgrw = pickle.load( open( "devices/"+self.device+"/"+"leds.leds", "rb" ))
		wc.well_restore(wellnumber,bgrw)
		
		return measurement

if __name__ == '__main__':
	#selecting device. If no device is given a promt will be produced in terminal
	if len(sys.argv) >2:
		device = (sys.argv[2])
	else:
		device = raw_input("Select device number: ")
	print "Running OD measurement for", experiment
	#set COMports for driver.
	wc = wellcontroller.wellcontroller(device)
	OD = ODmeasure()
	measurements = [None]*48
	# subtract disabled wells from total number of wells.
	wells = wc.activewell(range(1,49))
	#run measurements for all active wells wells.
	print "measuring..."
	for i in wells:
		measurements[i-1] = OD.OD_pulse(i,wc,device) 
	print "... done"
	#store measurements
	print "storing data..."
	csv = wellplate24csv.csvfileedit()
	csv.appendrow(experiment,'OD', measurements)
	print "... done"