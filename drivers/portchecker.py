#!/usr/bin/env python
"""
returns all comports currently in use
"""
__author__ = 'Bruno Favie  <bruno.favie@student.uva.nl>'
__version__ = "2015:12:04 13:00"

import wellcontrol
result = wellcontrol.scan_serial_ports()
print result
raw_input("press key to exit")