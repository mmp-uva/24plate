#!/usr/bin/env python
"""
Script containing the basic functions for controlling the calibration plate. The calibration plate covers 24 wells of the microplate incubator. Retunes raw value with sensor correction.
"""

__author__ = 'Bruno Favie <bruno.favie@student.uva.nl>'
__version__ = "2016:01:14 11:00"

import sys
import u6 as u6

"""
Each photodiode is adressed with a combination of an analog input connector (AIN), followed by 3 binary numbers for the multiplexor setting.
format = [AIN,MIO 0,MIO 1,MIO 2]
"""

adress = [[6,0,0,0],[6,0,1,0],[6,1,0,0],[6,1,1,0],[6,0,0,1],[6,0,1,1],[6,1,0,1],[6,1,1,1],
[4,0,0,0],[4,0,1,0],[4,1,0,0],[4,1,1,0],[4,0,0,1],[4,0,1,1],[4,1,0,1],[4,1,1,1],
[5,0,0,0],[5,0,1,0],[5,1,0,0],[5,1,1,0],[5,0,0,1],[5,0,1,1],[5,1,0,1],[5,1,1,1]]


# Dictionary containing relative spectral sensitivity of each color, (with white light as the standard( valued 1 ). Used to compensate for difference in sensor sensitivity.
#...[b,g,r,w]
spectral = {"blue":1.5,"green":1.2,"red":0.98,"white":1}

#correction values of each photodiode
cor = [0.9903074095029819,1.0294659016282488,0.9897409410419616,0.9867228997057057,0.9903175470675439,1.0294346025307815,0.9897597465800799,0.9867365584609263,0.9952795104332961,0.9662854142099849,1.1097751444530077,1.0193798770512708,0.9953127899598854,0.9663045454679904,1.1097515018560982,1.019411526827499,0.9726224858451702,0.9782603998107282,0.9633468437511943,1.015298246491907,0.9725917539983344,0.9783010306622374,0.9633562655407147,1.015283785576613]

""" connecting to the labjack U6-pro and the microplate incubator leds."""

class board():
	"""
	class for instructing calibration plate
	"""


	try:
		global d
		d = u6.U6()
		print "Calibration plate connected"
		d.getCalibrationData()
		print "Calibration plate ready"
	except:
		sys.exit("Calibration plate not available")


	def measure(self,well,color = 'white'):
		"""
		returns the measurement value of the diode of a specific well, taking the light frequency into account due to relative spectral sensitivity. Will use white color settings if no color is given.
		@param well: well number 1..24
		@type  well: int
		@param color: catagory led measured
		@type color: string
		"""
		if color == "white" or color == "red":
			sensor = well -1
		elif color == 'green' or color == 'blue': 
			sensor = (4*(well/4)+(5-(well%4)))-1 # adjusts sensor selection for flipped over calibration plate.
		#selecting channel
		channel = adress[sensor]
		#updating IO ports 16,17 & 18
		d.setDOState(16, state = channel[1])
		d.setDOState(17, state = channel[2])
		d.setDOState(18, state = channel[3])
		# resolutionindex 12 gives an effective resolution of 7.5 microvolts. Measuring time is 159ms/sample. gainindex is 1x
		measurement = d.getAIN(channel[0],resolutionIndex = 12,gainIndex = 1)
		#applying correction to initial meaurement based on sensor used.
		value = measurement * cor[sensor-1]
		return value
		    

