"""
driver for hardware 24-Wellplate Incubator
Contains classes for interacting with Micro Mojo V3 FPGA Developement board.
"""
__author__ = 'Hans Gerritsen <hans.gerritsen@xs4all.nl>',\
             'Bruno Favie <bruno.favie@student.uva.nl>'
__version__ = "2015:12:17 10:00"


import time
from sys import platform
from serial import Serial, SerialException

#
# Convert (number, bitlength) to two's complement integer 
#
def getSignedNumber(number, bitLength):
		mask = (2 ** bitLength) - 1
		if number & (1 << (bitLength - 1)):
				return number | ~mask
		else:
				return number & mask

#
# Scan system serial ports, return a list with comports
#
def scan_serial_ports():
		"""
		Return a list of serial port used by the OS
		"""
		serial = ''
		if platform == "linux" or platform == "linux2":
			serial = '/dev/ttyACM'
		elif platform == "darwin":
				serial = 'COM'
		elif platform == "win32":
				serial = 'COM'
		ports = [(serial + str(i+1)) for i in range(256)]
		result = []
		for port in ports:
				try:
						s = Serial(port)
						s.close()
						result.append(port)
				except (OSError, SerialException):
						pass
		return result

#
# constrain(x, a, b)
# Description: Constrains a number to be within a range.
# Parameters
# x: the number to constrain, all data types
# a: the lower end of the range, all data types
# b: the upper end of the range, all data types
# Returns
# x: if x is between a and b
# a: if x is less than a
# b: if x is greater than b
#
def constrain(x, a, b):
		"""
			 Constrains a number to be within a range between a an b
			 if x is less than a return x = a,  if x is greater than b return b
		"""
		if x < a:
				return a
		elif b < x:
				return b
		else:
				return x

class wcontrol:
		""" well control class """

		# constants
		HEADER = 0x55
		ACK    = "*"
		# class variables
		board0 = None
		board1 = None
		board2 = None
		debug = False

		#
		# Connect serial connect for board0
		#
		def connectBoard0(self, ComPort = '', BaudRate = 9600, TimeOut = 5):
				""" Connect serial port for board0 """
				try:
						self.board0 = Serial(port = ComPort, baudrate = BaudRate, timeout = TimeOut)
						print "Board 0 - serialport", ComPort, "Connected"
						return True
				except:
						print "Board 0 - serialport", ComPort, "not available"
						return False

		#
		# Connect serial connection for board1
		#
		def connectBoard1(self, ComPort = '', BaudRate = 9600, TimeOut = 5):
				""" Connect serial port for board1 """
				try:
						self.board1 = Serial(port = ComPort, baudrate = BaudRate, timeout = TimeOut)
						print "Board 1 - serialport", ComPort, "Connected"
						return True
				except:
						print "Board 1 - serialport", ComPort, "not available"
						return False

		#
		# Connect serial connection for board2
		#
		def connectBoard2(self, ComPort = '', BaudRate = 9600, TimeOut = 5):
				""" Connect serial port for board2 """
				try:
						self.board2 = Serial(port = ComPort, baudrate = BaudRate, timeout = TimeOut)
						print "Board 2 - serialport", ComPort, "Connected"
						return True
				except:
						print "Board 2 - serialport", ComPort, "not available"
						return False

		#
		# Check serial connection
		#
		def checkConnection(self, board = 0):
				""" Check serial connection """
				if board == 0:
						return self.board0 is not None
				elif board == 1:
						return self.board1 is not None
				elif board == 2:
						return self.board2 is not None

		#
		# Close all active serial connections
		#
		def closeConnection(self):
				""" closes the active serial connections """
				if self.checkConnection(0):
					self.board0.close()
				if self.checkConnection(1):
					self.board1.close()
				if self.checkConnection(2):
					self.board2.close()

		#
		# return version <date reversed>-<time>
		#
		@staticmethod
		def getVersion():
				""" return version <date reversed> <time> """
				return __version__

		#
		# Get current time (seconds)
		#
		@staticmethod
		def getCurrentTime():
				""" Get current time """
				t = time.ctime(time.time())
				t = t[11:19]
				return t

		#
		# time stamp and message
		#
		def message(self, s):
				""" Display message with timestamp """
				print self.getCurrentTime(), '*', s

		#
		# Connect to board 0, 1 or 2 return handle to COM port
		#
		def connect(self, sys):
				"""  Connect to board 0, 1 or 2 return handle to COM port """
				if sys == 0:
						return self.board0
				elif sys == 1:
						return self.board1
				elif sys == 2:
						return self.board2
				else:
						return None
		#
		# Debug mode On
		#
		def debugOn(self):
				self.debug = True

		#
		# Debug mode Off
		#
		def debugOff(self):
				self.debug = False

		#
		# convert lednr 0..191 to board 0..2, device 0..7, channel 0..7
		#
		@staticmethod
		def led_to_bdc(lednr):
				""" convert lednr 0..191 to board 0..2, device 0..7, channel 0..7 """
				board = lednr / 64
				device = lednr / 8 - board * 8
				chan = lednr % 8
				return board, device, chan

		#
		# convert lednr 0..191 to board 0..2, device 0..63
		#
		@staticmethod
		def led_to_bl(lednr):
				""" convert lednr 0..191 to board 0..2, led 0..63 """
				board = lednr / 64
				device = 64
				return board, device

		#
		# convert board 0..2, device 0..7 and channel 0..7 to lednr 0..191
		#
		@staticmethod
		def bdc_to_led(board, device, chan):
				""" convert board 0..2,device 0..7 and channel 0..7 to lednr 0..191 """
				return board * 64 + device * 8 + chan

		#
		# convert ODlednr 0..47 to board 0..2, device 8..9, channel 0..7
		#
		@staticmethod
		def ODled_to_bdc(lednr):
				""" convert lednr 0..48 to board 0..2, device 0..7, channel 0..7 """
				board = lednr / 16
				device = (lednr / 8 - board * 2) + 8
				chan = lednr % 8
				return board, device, chan

		#
		# convert ODlednr 0..47 to board 0..2, device 0..16
		#
		@staticmethod
		def ODled_to_bd(lednr):
				""" convert lednr 0..48 to board 0..2, device 0..16"""
				board = lednr / 16
				device = lednr % 16
				return board, device

		#
		#convert sensor 0..48 to board 0..2, adc 0..1, diode 0..7
		#
		@staticmethod
		def sensor_to_bad(sensor):
				"""converts sensor to board, adc and diode. Ignores adc = 2"""
				board = sensor / 16
				adc = sensor / 8 - board * 2
				diode = sensor % 8
				return board, adc, diode

		#
		# convert board 0..2, device 8..9 and channel 0..7 to lednr 0..191
		#
		@staticmethod
		def devchan_to_ODled(board, device, chan):
				""" convert board 0..2 device 0..1 and channel 0..7 to lednr 0..47 """
				return board * 16 + (device - 8) * 8 + chan

		#
		# ledpwm - command code = 0x01
		#
		def ledpwm(self, board = 0, device = 0, value = 0):
				"""
				Set led PWM value 0..1023
				@param board: int 0..2
				@param device: int 0..7
				@param value: int 0..1023
				@return: int 1 or 0 for error
				"""
				sys = self.connect(board)
				if sys is None:
						if self.debug:
								print "ledpwm - board", board, "is not online"
						return -1
				device = abs(device)
				#value = abs(value)
				value = constrain(value, 0, 1023)
				if device > 63:
						if self.debug:
								print "address %i range error, valid range is [0..63]" % device
						return -1
				if self.debug:
						print "%s : ledpwm(board=%i,device=%i,value=%i)" % \
						      (self.getCurrentTime(), board, device, value)
				sys.write(chr(self.HEADER))
				sys.write(chr(device))
				lobyte = value & 0x00FF
				hibyte = (value & 0xFF00) >> 8
				sys.write(chr(hibyte))
				sys.write(chr(lobyte))
				sys.write(chr(0x01)) # command code = 0x01
				status = 1
				status = sys.read(1)
				if status <> self.ACK:
						if self.debug:
								print "ledpwm - no ack from controller"
						status = 0
				return status

		#
		# daccontrol - command code = 0x02
		#
		def daccontrol(self, board = 0, device = 0, chan = 0, value = 0):
				"""
				don't call this function directly
				@param board: int 0..2
				@param device: int 0..7
				@param chan: int 0..7
				@param value: int 0..4095
				@return: int 1 or 0 for error
				"""
				sys = self.connect(board)
				if sys is None:
						if self.debug:
								print "leddac - board", board, "is not online"
						return 0
				value = constrain(value, 0, 4095)
				lobyte = value & 0x00ff
				hibyte = ((value & 0xff00) >> 8) | (chan << 4)
				sys.write(chr(self.HEADER))
				sys.write(chr(device))
				sys.write(chr(hibyte))
				sys.write(chr(lobyte))
				sys.write(chr(0x02)) # command code = 0x02
				if self.debug:
					if chan != 10:
							print "%s : leddac(board=%i,device=%i,chan=%i,value=%i)" % \
							      (self.getCurrentTime(), board, device, chan, value)
				status = sys.read(1)
				if status <> self.ACK:
						if self.debug:
								print "leddac - no ack from controller"
						return 0
				else:
						return 1

		#
		#  leddac -      0..2,      0..7,        0..7,    0..4095    code = 0x02
		#
		def leddac(self, board = 0, device = 0, chan = 0, value = 0):
				"""
				Set DAC value 0..4095
				@param board: int 0..2
				@param device: int 0..7
				@param chan: int 0..7
				@param value: int 0..4095
				@return: int 1 or 0 for error
				"""
				""" Set leddac value 0..4095 """
				# set register
				status1 = self.daccontrol(board, device, chan, value)
				# update select DAC
				status2 = self.daccontrol(board, device, 10, 255)
				status3 = self.daccontrol(board, device, 10, 0)
				if status1 == 1 and status2 == 1 and status3 == 1:
						return 1
				else:
						return 0

		#
		# ledext - code = 0x04
		#
		def ledext(self, board = 0, device = 0, onoff = 0):
				"""
				Set led EXT on/off, device 0..15
				@param board: int 0..2
				@param device: int 0..7
				@param onoff: int 0 or 1
				@return: int 1 or 0 for error
				"""
				sys = self.connect(board)
				if sys is None:
						if self.debug:
								print "ledext - board", board, "is not online"
						return 0
				#device = abs(device)
				#onoff=abs(onoff)
				if onoff == 0:
						onoff = 1
				else:
						onoff = 0
				if device < 0 or device > 15:
						if self.debug:
								print "Address %i range error, valid range is [0..15]" % device
						return -1
				sys.write(chr(self.HEADER))
				sys.write(chr(device))
				lobyte = onoff
				hibyte = 0
				sys.write(chr(hibyte))
				sys.write(chr(lobyte))
				sys.write(chr(0x04)) # command code = 0x04
				if self.debug:
						print "%s : ledext(board=%i,device=%i,onoff=%i)" % \
						      (self.getCurrentTime(), board, device, onoff)
				status = sys.read(1)
				if status <> self.ACK:
						if self.debug:
								print "ledext - no ack from controller"
						return 0
				else:
						return 1

		#
		# sensor code = 0x08 - get 24bit sensor data, if error return -1
		#  sensor -      0..2,        0..2,      0..7
		def sensor(self, board = 0, adc = 0 , diode = 0):
				"""
				Get 24bit sensor data, if error return -1
				@param board: int 0..2
				@param adc: int 0..2
				@param diode: int 0..7
				@return: int 24 bit signed or -1 for error
				"""
				sys = self.connect(board)
				if sys is None:
						if self.debug:
								print "sensor - board", board, "is not online"
						return -1
				sensor_value = 0
				#device = abs(diode)
				device = constrain(diode, 0, 7)
				#adc = abs(adc)
				adc = constrain(adc, 0, 2)
				sys.write(chr(self.HEADER))
				sys.write(chr(device))
				sys.write(chr(adc))       # hibyte = adc [0, 1, 2]
				sys.write(chr(adc + 4))   # lobyte = adc [4, 5, 6]
				sys.write(chr(0x08)) # command code = 0x08
				if self.debug:
						print "%s : sensor(board=%i,device=%i)" % (self.getCurrentTime(), board, device)
				datahi = ord(sys.read(1))
				datamid = ord(sys.read(1))
				datalow = ord(sys.read(1))
				# read ACK character
				status = sys.read(1)
				if status <> self.ACK:
						if self.debug:
								print "sensor - no ack from controller"
						return -1
				else:
						adc_value = (datahi << 16) + (datamid << 8) + datalow
						return getSignedNumber(adc_value, 24)

		#
		# relax code = 0x10
		#
		def relax(self, board = 0, device = 5):
				"""
				relax function
				@param board: int 0..2
				@param device: int 0..15
				@return: int 16 bit timer or -1 for error
				"""
				sys = self.connect(board)
				if sys is None:
						if self.debug:
								print "relax - board", board, "is not online"
						return -1
				device = constrain(device, 0, 15)
				sys.write(chr(self.HEADER))
				sys.write(chr(device))
				sys.write(chr(0x00)) # hibyte
				sys.write(chr(0x00)) # lobyte
				sys.write(chr(0x10)) # command code = 0x10
				if self.debug:
						print "%s : relax(board = %i, device = %i)" % \
						      (self.getCurrentTime(), board, device)
				datahi = ord(sys.read(1))
				datalow = ord(sys.read(1))
				# read ACK character
				status = sys.read(1)
				if status <> self.ACK:
						if self.debug:
								print "sensor - no ack from controller"
						return -1
				else:
						timer_value = (datahi << 8) + datalow
						return timer_value
